const SHA256 = require('crypto-js/sha256');

class Block {
    // Index = Posición del bloque en la cadena.
    // Data = El contenido del bloque en la cadena.
    // previousHash = Valor del bloque anteior de la cadena encriptado o cifrado.
    // date = Fecha de creación del bloque.
    // hash = Validación como cadena válida.
    // nounce = Número aleatorio de la cadena.
    constructor(index, data, previousHash=''){
        this.index = index;
        this.data = data;
        this.previousHash = previousHash;
        this.nounce = 0;
        this.hash = this.createdHash();
    }

    createdHash(){
        const originalChain = `${this.index}|${this.data}|${this.date}|${this.nounce}`;
        return SHA256(originalChain).toString();
    }

    mine(diff){
        while(!this.hash.startsWith(diff)){
            this.nounce++;
            this.hash = this.createdHash();
        }
    }
}

module.exports = Block;